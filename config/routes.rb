Rails.application.routes.draw do

  get 'unamo/index'
  get 'unamo/index/:param', :to => 'unamo#index'
  root 'unamo#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
