class CreateGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :groups do |t|
      t.integer :groupId
      t.string :name
      t.references :campaign, foreign_key: true

      t.timestamps
    end
  end
end
