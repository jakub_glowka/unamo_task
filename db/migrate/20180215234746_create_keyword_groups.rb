class CreateKeywordGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :keyword_groups do |t|
      t.references :keyword, foreign_key: true
      t.references :group, foreign_key: true

      t.timestamps
    end
  end
end
