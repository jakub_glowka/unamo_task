class RemoveGroupsDependancyFromKeywords < ActiveRecord::Migration[5.1]
  def self.up
    remove_column :keywords, :groupId
  end
end
