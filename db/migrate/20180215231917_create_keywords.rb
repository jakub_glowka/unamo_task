class CreateKeywords < ActiveRecord::Migration[5.1]
  def change
    create_table :keywords do |t|
      t.string :name
      t.references :campaign, foreign_key: true
      t.references :group, foreign_key: true

      t.timestamps
    end
  end
end
