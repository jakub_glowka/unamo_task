class RemoveColumns < ActiveRecord::Migration[5.1]
  def self.up
    remove_column :groups, :groupId
    remove_column :campaigns, :campaignId
  end
end
