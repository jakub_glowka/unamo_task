class ChangeDataTypeInPosition < ActiveRecord::Migration[5.1]
  def self.up
    remove_column :positions, :date
  end
  def change
    add_column :positions, :positions_date, :date
  end
end
