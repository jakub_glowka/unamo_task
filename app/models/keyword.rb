class Keyword < ApplicationRecord
  belongs_to :campaign
  has_many :keyword_groups
  has_many :groups, through: :keyword_groups
  has_many :positions
  def self.sync_from_api(campaign_id)
    UnamoApi.get_keywords(campaign_id)
  end
end
