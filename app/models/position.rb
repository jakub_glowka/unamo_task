class Position < ApplicationRecord
  belongs_to :keyword
  def self.sync_from_api(campaign_id, date_from, date_to)
    UnamoApi.get_positions(campaign_id, date_from, date_to)
  end
end
