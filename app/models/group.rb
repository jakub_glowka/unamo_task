class Group < ApplicationRecord
  belongs_to :campaign
  def self.sync_from_api(campaign_id)
    UnamoApi.get_groups(campaign_id)
  end
end
