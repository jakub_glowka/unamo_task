
class UnamoApi

  def self.testconnection
    response = Faraday.get 'https://seo.unamo.com/api/v1/?token='+ENV["AUTH_TOKEN"]

  end

  def self.get_campaigns

    response = Faraday.get 'https://seo.unamo.com/api/v1/campaigns.json?token='+ENV["AUTH_TOKEN"]

    if response.body
      campaigns = JSON.parse(response.body)
      campaigns.each do |campaign_api|
        c = Campaign.find_or_initialize_by(id: campaign_api['id'])
        c.name = campaign_api['name']
        c.id = campaign_api['id']
        c.save
      end
    end
  end

  def self.get_groups(campaign_id)

    response = Faraday.get 'https://seo.unamo.com/api/v1/campaigns/'+campaign_id.to_s+'/groups.json?token='+ENV["AUTH_TOKEN"]

    if response.body
      groups = JSON.parse(response.body)
      groups.each do |group_api|
        g = Group.find_or_initialize_by(id: group_api['id'])
        g.name = group_api['name']
        g.id = group_api['id']
        g.campaign_id = campaign_id
        g.save
      end
    end
  end

  def self.get_keywords(campaign_id)

    response = Faraday.get 'https://seo.unamo.com/api/v1/campaigns/'+campaign_id.to_s+'/keywords.json?token='+ENV["AUTH_TOKEN"]

    if response.body
      keywords = JSON.parse(response.body)
      keywords.each do |keyword_api|
        k = Keyword.find_or_initialize_by(id: keyword_api['id'])
        k.name = keyword_api['name']
        k.id = keyword_api['id']
        k.campaign_id = campaign_id
        k.save
        groups = keyword_api['groups']
        groups.each do |group_api|
          g = KeywordGroup.find_or_initialize_by(group_id: group_api, keyword_id: keyword_api['id'])
          g.group_id = group_api
          g.keyword_id = keyword_api['id']
          g.save
        end
      end
    end
  end

  def self.get_positions(campaign_id, date_from, date_to)
    response = Faraday.get 'https://seo.unamo.com/api/v1/campaigns/'+campaign_id.to_s+'/positions.json?date_from='+date_from.to_s+'&date_to='+date_to.to_s+'&token='+ENV["AUTH_TOKEN"]
    if response.body
      keyword = JSON.parse(response.body)
      keyword = keyword['keywords']
      keyword.each do |keyword_api|
        position = keyword_api['positions']
        position.each do |position_api|
          p = Position.find_or_initialize_by(id_keyword_date: keyword_api['id'].to_s+'_'+position_api[0])
          p.keyword_id = keyword_api['id']
          p.id_keyword_date = keyword_api['id'].to_s+'_'+position_api[0]
          p.position = position_api[1]
          p.positions_date = position_api[0].to_date
          p.save
          p(position_api[0])
          p('test')
          p(position_api[0].to_date)
        end
      end
    end
  end
end