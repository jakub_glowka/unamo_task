class UnamoController < ActionController::Base


  def index ()
    @period = (params[:param])


    @date_to = Date.today()
    case @period
      when '1week'
        @date_from = @date_to.advance(days:-7)
        @period_text = '1 week'
      when '2weeks'
        @date_from = @date_to.advance(days:-14)
        @period_text = '2 weeks'
      else
        @date_from = @date_to.advance(months:-1)
        @period_text = '1 month'
    end

    Campaign.sync_from_api
    @campaigns=Campaign.all
    @campaigns.each do |campaign|
      Group.sync_from_api(campaign.id)
      Keyword.sync_from_api(campaign.id)
      Position.sync_from_api(campaign.id, @date_from.to_s, @date_to.to_s)
    end
    @keywords=Keyword.all
  end
end

